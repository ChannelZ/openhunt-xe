/*
===========================================================================
based on OpenArena's ui_votemenu_map.c and Quake3's ui_startserver.c
===========================================================================
*/

#include "ui_local.h"

#define ART_BACK0			"menu/art_blueish/back_0"
#define ART_BACK1			"menu/art_blueish/back_1"
#define ART_FIGHT0			"menu/art_blueish/accept_0"
#define ART_FIGHT1			"menu/art_blueish/accept_1"
//#define ART_BACKGROUND                  "menu/art_blueish/addbotframe"
#define ART_ARROWS			"menu/art_blueish/arrows_vert_0"
#define ART_ARROWUP			"menu/art_blueish/arrows_vert_top"
#define ART_ARROWDOWN                   "menu/art_blueish/arrows_vert_bot"

#define VOTEMAP_SELECT			"menu/art/maps_select"
#define VOTEMAP_SELECTED		"menu/art/maps_selected"
#define VOTEMAP_UNKNOWNMAP		"menu/art/unknownmap"

#define ID_BACK				10
#define ID_GO				11
#define ID_LIST				12
#define ID_UP				13
#define ID_DOWN				14
#define ID_PICTURES			15

#define SIZE_OF_LIST                    12

#define SIZE_OF_NAME                    32
#define MAX_NAMELENGTH			(SIZE_OF_NAME)

#define VOTEMAP_MENU_VERTICAL_SPACING	20

#define MAXROW		3
#define MAXCOL		4
#define MAXMAPS		(MAPROWS * MAPCOLS)

typedef struct {
	menuframework_s	menu;
	menubitmap_s	arrows;
	menutext_s		banner;
	menutext_s		info;
	menubitmap_s	up;
	menubitmap_s	down;
	menubitmap_s	go;
	menubitmap_s	back;

	menubitmap_s    mappics[SIZE_OF_LIST];
	menubitmap_s    mapbuttons[SIZE_OF_LIST];

	menutext_s	maps[SIZE_OF_LIST];

	int		selected;
	//char		mapname[128];
} votemenu_map_t;

static votemenu_map_t	s_votemenu_map;

void UI_VoteMapMenuInternal(void);

/*
=================
VoteMapMenu_Event
=================
*/
static void VoteMapMenu_Event( void* ptr, int event ) {
	switch (((menucommon_s*)ptr)->id) {
		case ID_BACK:
			if (event != QM_ACTIVATED)
				return;
			UI_PopMenu();
			break;
		case ID_GO:
			if( event != QM_ACTIVATED ) {
				return;
			}
			//if(!s_votemenu_map.selected || mappage.mapname[s_votemenu_map.selected][0] == 0)
			if(s_votemenu_map.selected < 0 || mappage.mapname[s_votemenu_map.selected][0] == 0)
				return;
			if(!Q_stricmp(mappage.mapname[s_votemenu_map.selected], "---"))
				return; //Blank spaces have string "---"

			trap_Cmd_ExecuteText( EXEC_APPEND, va("callvote map %s\n", mappage.mapname[s_votemenu_map.selected]) );
			UI_PopMenu();
			UI_PopMenu();
			break;
		default:
			if( event != QM_ACTIVATED ) {
				return;
			}
			if(s_votemenu_map.selected != ((menucommon_s*)ptr)->id) {
				s_votemenu_map.selected = ((menucommon_s*)ptr)->id;
				UI_VoteMapMenuInternal();
			}
			break;
	}
}

/*
=================
UI_VoteMapMenu_UpEvent
=================
*/
static void UI_VoteMapMenu_UpEvent( void* ptr, int event ) {
	if (event != QM_ACTIVATED  || mappage.pagenumber<1) {
		return;
	}

        trap_Cmd_ExecuteText( EXEC_APPEND,va("getmappageui %d",mappage.pagenumber-1) );
}

/*
=================
UI_VoteMapMenu_DownEvent
=================
*/
static void UI_VoteMapMenu_DownEvent( void* ptr, int event ) {
	if (event != QM_ACTIVATED) {
		return;
	}

        trap_Cmd_ExecuteText( EXEC_APPEND,va("getmappageui %d",mappage.pagenumber+1) );
}

/*
=================
UI_VoteMapMenu_Draw
=================
*/
static void UI_VoteMapMenu_Draw( void ) {
	float color[4];

	color[0] = color[1] = color[2] = 0;
	color[3] = 0.8f;

	UI_SetColor( color );
	UI_DrawNamedPic( 40, 0, 560, 480, "white" );
	UI_SetColor( NULL );

	// standard menu drawing
	Menu_Draw( &s_votemenu_map.menu );
}

/*
===============
VoteMapMenu_LevelshotDraw
===============
*/
static void VoteMapMenu_LevelshotDraw( void *self ) {
	menubitmap_s    *b;
	int                             x;
	int                             y;
	int                             w;
	int                             h;
	int                             n;

	const char              *info;
	char                    mapname[ MAX_NAMELENGTH ];
	char			lshot[ MAX_NAMELENGTH + 9 ]; //levelshots/<mapname>.tga

	b = (menubitmap_s *)self;

	n = b->generic.id - ID_PICTURES;

	Com_sprintf(lshot, sizeof(lshot), "levelshots/%s.tga", mappage.mapname[n]); 

	b->shader = trap_R_RegisterShaderNoMip( lshot );

	if( b->generic.name && !b->shader ) {
		b->shader = trap_R_RegisterShaderNoMip( b->generic.name );
		if( !b->shader && b->errorpic ) {
			b->shader = trap_R_RegisterShaderNoMip( b->errorpic );
		}
	}

	if( b->focuspic && !b->focusshader ) {
		b->focusshader = trap_R_RegisterShaderNoMip( b->focuspic );
		//s_votemenu_map.selected = b->generic.id;
	}

	x = b->generic.x;
	y = b->generic.y;
	w = b->width;
	h = b->height;
	if( b->shader ) {
		UI_DrawHandlePic( x, y, w, h, b->shader );
	}

	x = b->generic.x;
	y = b->generic.y + b->height;
	UI_FillRect( x, y, b->width, 28, colorBlack );

	x += b->width / 2;
	y += 4;

	Q_strncpyz( mapname, mappage.mapname[n], MAX_NAMELENGTH );
	Q_strupr( mapname );
	if( b->generic.flags & QMF_HIGHLIGHT )
		UI_DrawString( x, y, mapname, UI_CENTER|UI_SMALLFONT, color_white );
	else
		UI_DrawString( x, y, mapname, UI_CENTER|UI_SMALLFONT, color_orange );

	x = b->generic.x;
	y = b->generic.y;
	w = b->width;
	h =     b->height + 28;
	if( b->generic.flags & QMF_HIGHLIGHT ) {
		UI_DrawHandlePic( x, y, w, h, b->focusshader );
	}
}

/*
=================
VoteMapMenu_MapUpdate
=================
*/
static void VoteMapMenu_MapUpdate( void ) {
	int i;

	for(i = 0; i < SIZE_OF_LIST; i++){
		if(i != s_votemenu_map.selected){
                	s_votemenu_map.mappics[i].generic.flags &= ~((unsigned int)QMF_HIGHLIGHT);
		} else {
			s_votemenu_map.mappics[i].generic.flags |= ((unsigned int)QMF_HIGHLIGHT);
		}
	}
}

/*
=================
VoteMapMenu_MapEvent
=================
*/
static void VoteMapMenu_MapEvent( void* ptr, int event ) {
        if( event != QM_ACTIVATED) { // it's a click?
                return;
        }

	s_votemenu_map.selected = ((menucommon_s*)ptr)->id - ID_PICTURES;

	VoteMapMenu_MapUpdate();
}

/*
=================
VoteMapMenu_Cache
=================
*/
static void VoteMapMenu_Cache( void )
{
	trap_R_RegisterShaderNoMip( ART_BACK0 );
	trap_R_RegisterShaderNoMip( ART_BACK1 );
	trap_R_RegisterShaderNoMip( ART_FIGHT0 );
	trap_R_RegisterShaderNoMip( ART_FIGHT1 );
	trap_R_RegisterShaderNoMip( ART_ARROWS );
	trap_R_RegisterShaderNoMip( ART_ARROWUP );
	trap_R_RegisterShaderNoMip( ART_ARROWDOWN );
}

/*
=================
UI_VoteMapMenuInternal
 *Used then forcing a redraw
=================
*/
void UI_VoteMapMenuInternal( void ) {
    int x, y, i;

    s_votemenu_map.menu.wrapAround = qtrue;
    s_votemenu_map.menu.fullscreen = qfalse;
    s_votemenu_map.menu.draw = UI_VoteMapMenu_Draw;


    s_votemenu_map.banner.generic.type	= MTYPE_TEXT;
    s_votemenu_map.banner.generic.x	= 320;
    s_votemenu_map.banner.generic.y	= 0;
    s_votemenu_map.banner.string	= "CALL A MAP VOTE";
    s_votemenu_map.banner.color		= color_white;
    s_votemenu_map.banner.style		= UI_CENTER;

    s_votemenu_map.info.generic.type	= MTYPE_TEXT;
    s_votemenu_map.info.generic.x	= 530;
    s_votemenu_map.info.generic.y	= 0;
    s_votemenu_map.info.string		= va("Page %d",mappage.pagenumber+1);
    s_votemenu_map.info.color		= color_white;
    s_votemenu_map.info.style		= UI_CENTER;

    s_votemenu_map.arrows.generic.type	= MTYPE_BITMAP;
    s_votemenu_map.arrows.generic.name	= ART_ARROWS;
    s_votemenu_map.arrows.generic.flags	= QMF_INACTIVE;
    s_votemenu_map.arrows.generic.x	= 640-40;
    s_votemenu_map.arrows.generic.y	= 128+30;
    s_votemenu_map.arrows.width		= 64;
    s_votemenu_map.arrows.height	= 128;

    y = 98;

    for (i=0; i < SIZE_OF_LIST; i++) {
                x = (i % MAXCOL) * (128+8) + 52;
                y = (i / MAXCOL) * (128+8) + 20;

                s_votemenu_map.mappics[i].generic.type   = MTYPE_BITMAP;
                s_votemenu_map.mappics[i].generic.flags  = QMF_LEFT_JUSTIFY|QMF_INACTIVE;
                s_votemenu_map.mappics[i].generic.x      = x;
                s_votemenu_map.mappics[i].generic.y      = y;
                s_votemenu_map.mappics[i].generic.id         = ID_PICTURES+i;
                s_votemenu_map.mappics[i].width              = 128;
                s_votemenu_map.mappics[i].height         = 96;
                s_votemenu_map.mappics[i].focuspic       = VOTEMAP_SELECTED;
                s_votemenu_map.mappics[i].errorpic       = VOTEMAP_UNKNOWNMAP;
                s_votemenu_map.mappics[i].generic.ownerdraw = VoteMapMenu_LevelshotDraw;

                s_votemenu_map.mapbuttons[i].generic.type     = MTYPE_BITMAP;
                s_votemenu_map.mapbuttons[i].generic.flags    = QMF_LEFT_JUSTIFY|QMF_PULSEIFFOCUS|QMF_NODEFAULTINIT;
                s_votemenu_map.mapbuttons[i].generic.id       = ID_PICTURES+i;
                s_votemenu_map.mapbuttons[i].generic.callback = VoteMapMenu_MapEvent;
                s_votemenu_map.mapbuttons[i].generic.x            = x - 30;
                s_votemenu_map.mapbuttons[i].generic.y            = y - 32;
                s_votemenu_map.mapbuttons[i].width                = 256;
                s_votemenu_map.mapbuttons[i].height               = 248;
                s_votemenu_map.mapbuttons[i].generic.left     = x;
                s_votemenu_map.mapbuttons[i].generic.top      = y;
                s_votemenu_map.mapbuttons[i].generic.right    = x + 128;
                s_votemenu_map.mapbuttons[i].generic.bottom   = y + 128;
                s_votemenu_map.mapbuttons[i].focuspic         = VOTEMAP_SELECT;
    }
    

    y+=VOTEMAP_MENU_VERTICAL_SPACING+8;

    s_votemenu_map.up.generic.type	    = MTYPE_BITMAP;
    s_votemenu_map.up.generic.flags    = QMF_LEFT_JUSTIFY|QMF_PULSEIFFOCUS;
    s_votemenu_map.up.generic.x		= 640-40;
    s_votemenu_map.up.generic.y		= 128+30;
    s_votemenu_map.up.generic.id	    = ID_UP;
    s_votemenu_map.up.generic.callback = UI_VoteMapMenu_UpEvent;
    s_votemenu_map.up.width  		    = 64;
    s_votemenu_map.up.height  		    = 64;
    s_votemenu_map.up.focuspic         = ART_ARROWUP;

    s_votemenu_map.down.generic.type	  = MTYPE_BITMAP;
    s_votemenu_map.down.generic.flags    = QMF_LEFT_JUSTIFY|QMF_PULSEIFFOCUS;
    s_votemenu_map.down.generic.x		  = 640-40;
    s_votemenu_map.down.generic.y		  = 128+30+64;
    s_votemenu_map.down.generic.id	      = ID_DOWN;
    s_votemenu_map.down.generic.callback = UI_VoteMapMenu_DownEvent;
    s_votemenu_map.down.width  		  = 64;
    s_votemenu_map.down.height  		  = 64;
    s_votemenu_map.down.focuspic         = ART_ARROWDOWN;

    s_votemenu_map.go.generic.type			= MTYPE_BITMAP;
    s_votemenu_map.go.generic.name			= ART_FIGHT0;
    s_votemenu_map.go.generic.flags		= QMF_LEFT_JUSTIFY|QMF_PULSEIFFOCUS;
    s_votemenu_map.go.generic.id			= ID_GO;
    s_votemenu_map.go.generic.callback		= VoteMapMenu_Event;
    s_votemenu_map.go.generic.x			= 320+128-128;
    s_votemenu_map.go.generic.y			= 424; //256+128-64;
    s_votemenu_map.go.width  				= 128;
    s_votemenu_map.go.height  				= 64;
    s_votemenu_map.go.focuspic				= ART_FIGHT1;


    s_votemenu_map.back.generic.type		= MTYPE_BITMAP;
    s_votemenu_map.back.generic.name		= ART_BACK0;
    s_votemenu_map.back.generic.flags		= QMF_LEFT_JUSTIFY|QMF_PULSEIFFOCUS;
    s_votemenu_map.back.generic.id			= ID_BACK;
    s_votemenu_map.back.generic.callback	= VoteMapMenu_Event;
    s_votemenu_map.back.generic.x			= 320-128;
    s_votemenu_map.back.generic.y			= 424; //256+128-64;
    s_votemenu_map.back.width				= 128;
    s_votemenu_map.back.height				= 64;
    s_votemenu_map.back.focuspic			= ART_BACK1;
}

t_mappage mappage;

/*
=================
UI_VoteMapMenu
 *Called from outside
=================
*/
void UI_VoteMapMenu( void ) {
    int i;
    VoteMapMenu_Cache();
    memset( &s_votemenu_map, 0 ,sizeof(votemenu_map_t) );

    UI_VoteMapMenuInternal();

    //We need to initialize the list or it will be impossible to click on the items
    for(i=0;i<SIZE_OF_LIST;i++) {
        Q_strncpyz(mappage.mapname[i],"----",5);
    }
    trap_Cmd_ExecuteText( EXEC_APPEND,"getmappageui 0" );
    trap_Cvar_Set( "cl_paused", "0" ); //We cannot send server commands while paused!
    Menu_AddItem( &s_votemenu_map.menu, (void*) &s_votemenu_map.banner );
    Menu_AddItem( &s_votemenu_map.menu, (void*) &s_votemenu_map.info );
    Menu_AddItem( &s_votemenu_map.menu, (void*) &s_votemenu_map.back );
    Menu_AddItem( &s_votemenu_map.menu, (void*) &s_votemenu_map.go );
    Menu_AddItem( &s_votemenu_map.menu, (void*) &s_votemenu_map.arrows );
    Menu_AddItem( &s_votemenu_map.menu, (void*) &s_votemenu_map.down );
    Menu_AddItem( &s_votemenu_map.menu, (void*) &s_votemenu_map.up );
    for(i=0;i<SIZE_OF_LIST;i++) {
        //Menu_AddItem( &s_votemenu_map.menu, (void*) &s_votemenu_map.maps[i] );
	Menu_AddItem( &s_votemenu_map.menu, (void*) &s_votemenu_map.mappics[i] );
	Menu_AddItem( &s_votemenu_map.menu, (void*) &s_votemenu_map.mapbuttons[i] );
    }

    UI_PushMenu( &s_votemenu_map.menu );
}
