// Copyright (C) 1999-2000 Id Software, Inc.
//
/*
=============================================================================

VOTE MAP MENU *****
( based on ui_startserver.c )

=============================================================================
*/


#include "ui_local.h"


#define VOTEMAP_BACK0		"menu/art/back_0"
#define VOTEMAP_BACK1		"menu/art/back_1"
#define VOTEMAP_NEXT0		"menu/art/next_0"
#define VOTEMAP_NEXT1		"menu/art/next_1"
#define VOTEMAP_FRAMEL		"menu/art/frame2_l"
#define VOTEMAP_FRAMER		"menu/art/frame1_r"
#define VOTEMAP_SELECT		"menu/art/maps_select"
#define VOTEMAP_SELECTED	"menu/art/maps_selected"
#define VOTEMAP_FIGHT0		"menu/art/fight_0"
#define VOTEMAP_FIGHT1		"menu/art/fight_1"
#define VOTEMAP_UNKNOWNMAP	"menu/art/unknownmap"
#define VOTEMAP_ARROWS		"menu/art/gs_arrows_0"
#define VOTEMAP_ARROWSL		"menu/art/gs_arrows_l"
#define VOTEMAP_ARROWSR		"menu/art/gs_arrows_r"

#define MAX_MAPROWS		3
#define MAX_MAPCOLS		4
#define MAX_MAPSPERPAGE		(MAX_MAPROWS * MAX_MAPCOLS)

#define MAX_NAMELENGTH	16
#define ID_GAMETYPE				10
#define ID_PICTURES				11	// 12, 13, 14
#define ID_PREVPAGE				15
#define ID_NEXTPAGE				16
#define ID_ABORT		17
#define ID_VOTEMAP		18

typedef struct {
	menuframework_s	menu;

	menutext_s		banner;
	menubitmap_s	framel;
	menubitmap_s	framer;

	menubitmap_s	mappics[MAX_MAPSPERPAGE];
	menubitmap_s	mapbuttons[MAX_MAPSPERPAGE];
	menubitmap_s	arrows;
	menubitmap_s	prevpage;
	menubitmap_s	nextpage;
	menubitmap_s	back;
	menubitmap_s	next;

	menutext_s		mapname;
	menubitmap_s	item_null;

	qboolean		multiplayer;
	int				currentmap;
	int				nummaps;
	int				page;
	int				maxpages;
	int 			maplist[MAX_ARENAS];
} votemap_t;

static votemap_t s_votemap;


// use ui_servers2.c definition
//extern const char* punkbuster_items[];

static void UI_ServerOptionsMenu( qboolean multiplayer );


/*
=================
VoteMap_Update
=================
*/
static void VoteMap_Update( void ) {
	int				i;
	int				top;
	static	char	picname[MAX_MAPSPERPAGE][64];
	const char		*info;
	char			mapname[MAX_NAMELENGTH];

	top = s_votemap.page*MAX_MAPSPERPAGE;

	for (i=0; i<MAX_MAPSPERPAGE; i++)
	{
		if (top+i >= s_votemap.nummaps)
			break;
		
		info = UI_GetArenaInfoByNumber( s_votemap.maplist[ top + i ]);
		Q_strncpyz( mapname, Info_ValueForKey( info, "map"), MAX_NAMELENGTH );
		Q_strupr( mapname );

		Com_sprintf( picname[i], sizeof(picname[i]), "levelshots/%s", mapname );

		s_votemap.mappics[i].generic.flags &= ~QMF_HIGHLIGHT;
		s_votemap.mappics[i].generic.name   = picname[i];
		s_votemap.mappics[i].shader         = 0;

		// reset
		s_votemap.mapbuttons[i].generic.flags |= QMF_PULSEIFFOCUS;
		s_votemap.mapbuttons[i].generic.flags &= ~QMF_INACTIVE;
	}

	for (; i<MAX_MAPSPERPAGE; i++)
	{
		s_votemap.mappics[i].generic.flags &= ~QMF_HIGHLIGHT;
		s_votemap.mappics[i].generic.name   = NULL;
		s_votemap.mappics[i].shader         = 0;

		// disable
		s_votemap.mapbuttons[i].generic.flags &= ~QMF_PULSEIFFOCUS;
		s_votemap.mapbuttons[i].generic.flags |= QMF_INACTIVE;
	}


	// no servers to start
	if( !s_votemap.nummaps ) {
		s_votemap.next.generic.flags |= QMF_INACTIVE;

		// set the map name
		strcpy( s_votemap.mapname.string, "NO MAPS FOUND" );
	}
	else {
		// set the highlight
		s_votemap.next.generic.flags &= ~QMF_INACTIVE;
		i = s_votemap.currentmap - top;
		if ( i >=0 && i < MAX_MAPSPERPAGE ) 
		{
			s_votemap.mappics[i].generic.flags    |= QMF_HIGHLIGHT;
			s_votemap.mapbuttons[i].generic.flags &= ~QMF_PULSEIFFOCUS;
		}

		// set the map name
		info = UI_GetArenaInfoByNumber( s_votemap.maplist[ s_votemap.currentmap ]);
		Q_strncpyz( s_votemap.mapname.string, Info_ValueForKey( info, "map" ), MAX_NAMELENGTH);
	}
	
	Q_strupr( s_votemap.mapname.string );
}


/*
=================
VoteMap_MapEvent
=================
*/
static void VoteMap_MapEvent( void* ptr, int event ) {
	if( event != QM_ACTIVATED) {
		return;
	}

	s_votemap.currentmap = (s_votemap.page*MAX_MAPSPERPAGE) + (((menucommon_s*)ptr)->id - ID_PICTURES);
	VoteMap_Update();
}


/*
=================
VoteMap_MenuEvent
=================
*/
static void VoteMap_MenuEvent( void* ptr, int event ) {
	if( event != QM_ACTIVATED ) {
		return;
	}

	switch( ((menucommon_s*)ptr)->id ) {
	case ID_PREVPAGE:
		if( s_votemap.page > 0 ) {
			s_votemap.page--;
			VoteMap_Update();
		}
		break;

	case ID_NEXTPAGE:
		if( s_votemap.page < s_votemap.maxpages - 1 ) {
			s_votemap.page++;
			VoteMap_Update();
		}
		break;

	case ID_VOTEMAP:
		//trap_Cvar_SetValue( "g_gameType", gametype_remap[s_votemap.gametype.curvalue] );
		UI_ServerOptionsMenu( s_votemap.multiplayer );
		break;

	case ID_ABORT:
		UI_PopMenu();
		break;
	}
}


/*
===============
VoteMap_LevelshotDraw
===============
*/
static void VoteMap_LevelshotDraw( void *self ) {
	menubitmap_s	*b;
	int				x;
	int				y;
	int				w;
	int				h;
	int				n;
	const char		*info;
	char			mapname[ MAX_NAMELENGTH ];

	b = (menubitmap_s *)self;

	if( !b->generic.name ) {
		return;
	}

	if( b->generic.name && !b->shader ) {
		b->shader = trap_R_RegisterShaderNoMip( b->generic.name );
		if( !b->shader && b->errorpic ) {
			b->shader = trap_R_RegisterShaderNoMip( b->errorpic );
		}
	}

	if( b->focuspic && !b->focusshader ) {
		b->focusshader = trap_R_RegisterShaderNoMip( b->focuspic );
	}

	x = b->generic.x;
	y = b->generic.y;
	w = b->width;
	h =	b->height;
	if( b->shader ) {
		UI_DrawHandlePic( x, y, w, h, b->shader );
	}

	x = b->generic.x;
	y = b->generic.y + b->height;
	UI_FillRect( x, y, b->width, 28, colorBlack );

	x += b->width / 2;
	y += 4;
	n = s_votemap.page * MAX_MAPSPERPAGE + b->generic.id - ID_PICTURES;

	info = UI_GetArenaInfoByNumber( s_votemap.maplist[ n ]);
	Q_strncpyz( mapname, Info_ValueForKey( info, "map"), MAX_NAMELENGTH );
	Q_strupr( mapname );
	UI_DrawString( x, y, mapname, UI_CENTER|UI_SMALLFONT, color_orange );

	x = b->generic.x;
	y = b->generic.y;
	w = b->width;
	h =	b->height + 28;
	if( b->generic.flags & QMF_HIGHLIGHT ) {	
		UI_DrawHandlePic( x, y, w, h, b->focusshader );
	}
}


/*
=================
VoteMap_MenuInit
=================
*/
static void VoteMap_MenuInit( void ) {
	int	i;
	int	x;
	int	y;
	static char mapnamebuffer[64];

	// zero set all our globals
	memset( &s_votemap, 0 ,sizeof(votemap_t) );

	VoteMap_Cache();

	s_votemap.menu.wrapAround = qtrue;
	s_votemap.menu.fullscreen = qtrue;

	s_votemap.banner.generic.type  = MTYPE_BTEXT;
	s_votemap.banner.generic.x	   = 320;
	s_votemap.banner.generic.y	   = 16;
	s_votemap.banner.string        = "GAME SERVER";
	s_votemap.banner.color         = color_white;
	s_votemap.banner.style         = UI_CENTER;

	s_votemap.framel.generic.type  = MTYPE_BITMAP;
	s_votemap.framel.generic.name  = VOTEMAP_FRAMEL;
	s_votemap.framel.generic.flags = QMF_INACTIVE;
	s_votemap.framel.generic.x	   = 0;  
	s_votemap.framel.generic.y	   = 78;
	s_votemap.framel.width  	   = 256;
	s_votemap.framel.height  	   = 329;

	s_votemap.framer.generic.type  = MTYPE_BITMAP;
	s_votemap.framer.generic.name  = VOTEMAP_FRAMER;
	s_votemap.framer.generic.flags = QMF_INACTIVE;
	s_votemap.framer.generic.x	   = 376;
	s_votemap.framer.generic.y	   = 76;
	s_votemap.framer.width  	   = 256;
	s_votemap.framer.height  	   = 334;

	for (i=0; i<MAX_MAPSPERPAGE; i++)
	{
		x =	(i % MAX_MAPCOLS) * (128+8) + 188;
		y = (i / MAX_MAPROWS) * (128+8) + 96;

		s_votemap.mappics[i].generic.type   = MTYPE_BITMAP;
		s_votemap.mappics[i].generic.flags  = QMF_LEFT_JUSTIFY|QMF_INACTIVE;
		s_votemap.mappics[i].generic.x	    = x;
		s_votemap.mappics[i].generic.y	    = y;
		s_votemap.mappics[i].generic.id		= ID_PICTURES+i;
		s_votemap.mappics[i].width  		= 128;
		s_votemap.mappics[i].height  	    = 96;
		s_votemap.mappics[i].focuspic       = VOTEMAP_SELECTED;
		s_votemap.mappics[i].errorpic       = VOTEMAP_UNKNOWNMAP;
		s_votemap.mappics[i].generic.ownerdraw = VoteMap_LevelshotDraw;

		s_votemap.mapbuttons[i].generic.type     = MTYPE_BITMAP;
		s_votemap.mapbuttons[i].generic.flags    = QMF_LEFT_JUSTIFY|QMF_PULSEIFFOCUS|QMF_NODEFAULTINIT;
		s_votemap.mapbuttons[i].generic.id       = ID_PICTURES+i;
		s_votemap.mapbuttons[i].generic.callback = VoteMap_MapEvent;
		s_votemap.mapbuttons[i].generic.x	     = x - 30;
		s_votemap.mapbuttons[i].generic.y	     = y - 32;
		s_votemap.mapbuttons[i].width  		     = 256;
		s_votemap.mapbuttons[i].height  	     = 248;
		s_votemap.mapbuttons[i].generic.left     = x;
		s_votemap.mapbuttons[i].generic.top  	 = y;
		s_votemap.mapbuttons[i].generic.right    = x + 128;
		s_votemap.mapbuttons[i].generic.bottom   = y + 128;
		s_votemap.mapbuttons[i].focuspic         = VOTEMAP_SELECT;
	}

	s_votemap.arrows.generic.type  = MTYPE_BITMAP;
	s_votemap.arrows.generic.name  = VOTEMAP_ARROWS;
	s_votemap.arrows.generic.flags = QMF_INACTIVE;
	s_votemap.arrows.generic.x	   = 260;
	s_votemap.arrows.generic.y	   = 400;
	s_votemap.arrows.width  	   = 128;
	s_votemap.arrows.height  	   = 32;

	s_votemap.prevpage.generic.type	    = MTYPE_BITMAP;
	s_votemap.prevpage.generic.flags    = QMF_LEFT_JUSTIFY|QMF_PULSEIFFOCUS;
	s_votemap.prevpage.generic.callback = VoteMap_MenuEvent;
	s_votemap.prevpage.generic.id	    = ID_PREVPAGE;
	s_votemap.prevpage.generic.x		= 260;
	s_votemap.prevpage.generic.y		= 400;
	s_votemap.prevpage.width  		    = 64;
	s_votemap.prevpage.height  		    = 32;
	s_votemap.prevpage.focuspic         = VOTEMAP_ARROWSL;

	s_votemap.nextpage.generic.type	    = MTYPE_BITMAP;
	s_votemap.nextpage.generic.flags    = QMF_LEFT_JUSTIFY|QMF_PULSEIFFOCUS;
	s_votemap.nextpage.generic.callback = VoteMap_MenuEvent;
	s_votemap.nextpage.generic.id	    = ID_NEXTPAGE;
	s_votemap.nextpage.generic.x		= 321;
	s_votemap.nextpage.generic.y		= 400;
	s_votemap.nextpage.width  		    = 64;
	s_votemap.nextpage.height  		    = 32;
	s_votemap.nextpage.focuspic         = VOTEMAP_ARROWSR;

	s_votemap.mapname.generic.type  = MTYPE_PTEXT;
	s_votemap.mapname.generic.flags = QMF_CENTER_JUSTIFY|QMF_INACTIVE;
	s_votemap.mapname.generic.x	    = 320;
	s_votemap.mapname.generic.y	    = 440;
	s_votemap.mapname.string        = mapnamebuffer;
	s_votemap.mapname.style         = UI_CENTER|UI_BIGFONT;
	s_votemap.mapname.color         = text_color_normal;

	s_votemap.back.generic.type	    = MTYPE_BITMAP;
	s_votemap.back.generic.name     = VOTEMAP_BACK0;
	s_votemap.back.generic.flags    = QMF_LEFT_JUSTIFY|QMF_PULSEIFFOCUS;
	s_votemap.back.generic.callback = VoteMap_MenuEvent;
	s_votemap.back.generic.id	    = ID_STARTSERVERBACK;
	s_votemap.back.generic.x		= 0;
	s_votemap.back.generic.y		= 480-64;
	s_votemap.back.width  		    = 128;
	s_votemap.back.height  		    = 64;
	s_votemap.back.focuspic         = VOTEMAP_BACK1;

	s_votemap.next.generic.type	    = MTYPE_BITMAP;
	s_votemap.next.generic.name     = VOTEMAP_NEXT0;
	s_votemap.next.generic.flags    = QMF_RIGHT_JUSTIFY|QMF_PULSEIFFOCUS;
	s_votemap.next.generic.callback = VoteMap_MenuEvent;
	s_votemap.next.generic.id	    = ID_STARTSERVERNEXT;
	s_votemap.next.generic.x		= 640;
	s_votemap.next.generic.y		= 480-64;
	s_votemap.next.width  		    = 128;
	s_votemap.next.height  		    = 64;
	s_votemap.next.focuspic         = VOTEMAP_NEXT1;

	s_votemap.item_null.generic.type	= MTYPE_BITMAP;
	s_votemap.item_null.generic.flags	= QMF_LEFT_JUSTIFY|QMF_MOUSEONLY|QMF_SILENT;
	s_votemap.item_null.generic.x		= 0;
	s_votemap.item_null.generic.y		= 0;
	s_votemap.item_null.width			= 640;
	s_votemap.item_null.height			= 480;

	Menu_AddItem( &s_votemap.menu, &s_votemap.banner );
	Menu_AddItem( &s_votemap.menu, &s_votemap.framel );
	Menu_AddItem( &s_votemap.menu, &s_votemap.framer );

	for (i=0; i<MAX_MAPSPERPAGE; i++)
	{
		Menu_AddItem( &s_votemap.menu, &s_votemap.mappics[i] );
		Menu_AddItem( &s_votemap.menu, &s_votemap.mapbuttons[i] );
	}

	Menu_AddItem( &s_votemap.menu, &s_votemap.arrows );
	Menu_AddItem( &s_votemap.menu, &s_votemap.prevpage );
	Menu_AddItem( &s_votemap.menu, &s_votemap.nextpage );
	Menu_AddItem( &s_votemap.menu, &s_votemap.back );
	Menu_AddItem( &s_votemap.menu, &s_votemap.next );
	Menu_AddItem( &s_votemap.menu, &s_votemap.mapname );
	Menu_AddItem( &s_votemap.menu, &s_votemap.item_null );

	//VoteMap_GametypeEvent( NULL, QM_ACTIVATED );
}


/*
=================
VoteMap_Cache
=================
*/
void VoteMap_Cache( void )
{
	int				i;
	const char		*info;
	qboolean		precache;
	char			picname[64];
	char			mapname[ MAX_NAMELENGTH ];

	trap_R_RegisterShaderNoMip( VOTEMAP_BACK0 );	
	trap_R_RegisterShaderNoMip( VOTEMAP_BACK1 );	
	trap_R_RegisterShaderNoMip( VOTEMAP_NEXT0 );	
	trap_R_RegisterShaderNoMip( VOTEMAP_NEXT1 );	
	trap_R_RegisterShaderNoMip( VOTEMAP_FRAMEL );	
	trap_R_RegisterShaderNoMip( VOTEMAP_FRAMER );	
	trap_R_RegisterShaderNoMip( VOTEMAP_SELECT );	
	trap_R_RegisterShaderNoMip( VOTEMAP_SELECTED );	
	trap_R_RegisterShaderNoMip( VOTEMAP_UNKNOWNMAP );
	trap_R_RegisterShaderNoMip( VOTEMAP_ARROWS );
	trap_R_RegisterShaderNoMip( VOTEMAP_ARROWSL );
	trap_R_RegisterShaderNoMip( VOTEMAP_ARROWSR );

	precache = trap_Cvar_VariableValue("com_buildscript");

	if( precache ) {
		for( i = 0; i < UI_GetNumArenas(); i++ ) {
			info = UI_GetArenaInfoByNumber( i );
			Q_strncpyz( mapname, Info_ValueForKey( info, "map"), MAX_NAMELENGTH );
			Q_strupr( mapname );
	
			Com_sprintf( picname, sizeof(picname), "levelshots/%s", mapname );
			trap_R_RegisterShaderNoMip(picname);
		}
	}
}


/*
=================
UI_VoteMapMenu
=================
*/
void UI_StartServerMenu( qboolean multiplayer ) {
	StartServer_MenuInit();
	s_votemap.multiplayer = multiplayer;
	UI_PushMenu( &s_votemap.menu );
}



