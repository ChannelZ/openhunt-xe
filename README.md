# OpenHunt-Extended
-----------------

This is a fork of OpenHunt.
You can read what is was in the README-old.md file.
This fork mainly focuses on making it work on OpenArena and
making the weapons behave more like they do in Q3/OA,
but it can be switched back to the classic mode.
Also the TSS script stuff was removed/disabled,
I don't know what it was used for or if it was ever used?

### Usage
There is a pk3(openhunt-xe.pk3) with the QVMs inside you just drop into a mod folder like any other mod if you just want to use it as is.

### Compilation
First you will need q3asm q3lcc and so on in your path, you can use the ones you can compile from ioquake3.
Then in your terminal just:
`
bash$ ./compile
`
and after a little while you should have a fresh pk3 :)